# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.24.3] - 2021-07-23

### Fixed

- Fix "review required" screen from failing to advance to the next portal to review when selecting
  inn. The same code is called from the portal details screen and it was trying to update this
  screen's inn dropdown "color".  Since that dropdown doesn't exist here, it was failing and not
  advancing to the next portal.

## [0.24.2] - 2021-04-20

### Fixed

- Support IITC-CE v0.31.1.20210417.202019 where updateDisplayedLayerGroup was moved/removed
  and is now found in layerChooser._storeOverlayState.

## [0.24.1] - 2021-03-12

### Fixed

- Calculate the poi/inn scores for zooms < 15 for s2 level <= zoom

  Previously, we relied on intel's logic of showing all portals at zoom 15.
  We have the maximum mapped markers on screen limit to try to keep performance
  reasonable.

  Now, L11 S2 cell scores can be seen at zoom 11+.
  L12 S2 cell scores can be seen at zoom 12+.

- Fix disappearing inn/poi circle marker when changing "maximum markers on screen" limit.

## [0.24.0] - 2021-03-11

### Added

- Add option in action menu to export the visible S2 cell POI stats.

  This will require the POI Scores layer to be enabled.

## [0.23.8] - 2021-03-11

### Fixed

- Don't show new filters for portal history (visited, captured, scout controlled) in this is hpwu mode.

## [0.23.7] - 2021-02-02

### Fixed
- Fix missing N/A portals when loading json file.

  Redo mass rename to nothpwu from notHPWU.

  Our json storage uses the nothpwu key and it's easier to use that key as the new
  variable name all over the place than to try to find all places where the
  nothpwu key is used dynamically, such as traversing the keys in the loaded json
  file.

## [0.23.6] - 2021-02-02

### Fixed
- Fixed style for N/A button in portal details and classification dialogs following 0.23.4 mass rename

  0.23.4 mass renamed nothpwu to notHPWU and collided on CSS classes, causing
  the buttons for the N/A in the portal details and classification dialog to
  use the wrong CSS class.

## [0.23.5] - 2021-02-02

### Fixed
-  Fix detection of removed/missing N/A and pending portals.  There was commented code in place to do this,
   but was untested and an inconsistency of variable naming of nothpwu and notHPWU prevented this from working.
   After uncommenting the code and renaming these to notHPWU, it works!

## [0.23.4] - 2021-02-02

### Oops
- Forgot to properly tag the release. Use 0.23.5 instead.

## [0.23.3] - 2020-12-14

### Fixed
-  Fixed an issue where intel fails to load with iitc/hpwu plugin if ornament, beacon, or fracker layers are hidden (not checked).

## [0.23.2] - 2020-09-17

### Fixed
-  Only iterate through and therefore filter enabled layers.

## [0.23.1] - 2020-09-17

### Fixed
-  Update portal names if they've been renamed.

## [0.23.0] - 2020-09-03

### Added
-  Add keyboard shortcut support on the classify portals screen: f/g/i/n/p/left arrow/right arrow/escape.

### Changed
-  Because we now support keyboard shortcuts on both classify and portal details screens and both allows interactions
   outside of the dialog, such as moving the map, we now only allow one of these two to be opened at once.  Therefore,
   opening classify/review portal screen will close the portal details screen if open and vice versa.

### Fixed
-  Fix hover markers not appearing for portals without images.

## [0.22.2] - 2020-09-02

### Fixed

-  Removed last reference to grouped portals: fixes "not advancing to the next portal after clicking an icon"

## [0.22.1] - 2020-09-02

### Fixed

-  Classify portals independently, not as a s2 cell group.

   Fixes issues when several portals can be reviewed in the "Which one is in HPWU?"
   window as a group:

   * Mapping one as inn skips the others on screen and goes to the next group
   * Previous/next buttons intent is unclear when looking at several portals in a group
   * Cannot easily add keyboard shorcuts to mark a portal from the group as an 'inn'

   Splitting these dialogs to 1 portal per screen makes all the buttons make more
   sense and allows us to map f/i/g/n/p/escape/</> for the screen and they make a
   lot more sense.

-  Fix open/close portal details screen removing icons on 'which one is in hpwu' dialog

   Give the icons on the "which one is in hpwu" dialog a unique class name so when
   the portal details screen tries to clear the previous portal details icons, it
   doesn't accidentally clear the "which one is in hpwu" icons.

## [0.22.0] - 2020-09-01

### Added
-  Add a new and enabled layer for "pending" sync portals with shortcut key 'p' on portal details screen.
-  Added pending portals and total POI to the mapped statistics screen and adjusted layout to fit them.

### Changed
-  Enable N/A layer by default.
-  Change "Review required" screen from text to icons to enable all types to fit on both desktop and mobile.
-  Update the grid score and new/missing portal counters when changing a POI, which only works on desktop currently.

## [0.21.1] - 2020-08-26

### Fixed
-  Don't trigger changes to the mapped POI types when the user is typing in the search box or similar input/textareas.

## [0.21.0] - 2020-08-25

### Added
-  Add portal details keyboard shortcuts for mapping portals.
    When the details window is open:
    (f) toggles fortress
    (g) toggles greenhouse
    (i) toggles inn
    (n) toggles N/A
    (Escape) closes the portal details window

## [0.20.0] - 2020-08-11

### Added
- Small markers when zoomed out: fortresses, greenhouses, N/A (70% of original size) at zoom < 16.

### Changed
- Small markers when zoomed out: inns (70%, previously 80%) at zoom < 16.  Fortresses now have 0.9 opacity so they're slightly more transparent.

## [0.19.0] - 2020-08-10

### Added
- Portal review: N/A is now an option.

### Changed
- Portal review: skip button was removed in favor of using previous and next buttons to navigate unclassified portals.

### Fixed
- Portal review: Fixed a bug/typo that prevented classifying as a fortress.

## [0.18.0] - 2020-08-06

### Changed
- Settings dialog was changed to now only show "Maximum mapped markers on screen" as the zoom level filtering was not needed.  The number of markers we draw on screen is why performance gets bad and zoom level is only somewhat related.

### Fixed
- Status bar should now correctly show "Filtered View" on both desktop (working previously) and mobile (was previously broken) when the on screen mapped objects exceeds the limit specified in the setting discussed above.

## [0.17.0] - 2020-08-05

### Added
- Filter displayed markers based on limits set by the user. The settings menu was updated to allow the user to set a zoom level to start filtering objects and the hard limit
  for how many objects can be drawn at filtered zoomed levels.  Note, the status of filtering is displayed on desktop but doesn't appear to work yet on mobile.

## [0.16.2] - 2020-08-04

### Fixed
- Export isn't working on iphones on ios 13 and possibly earlier.  We now detect mobile AppleWebKit as a problematic platform for saving/loading and offer the ability to copy your data to the clipboard to paste into a new file in the "Files" app or to paste into the import dialog.  The import dialog now optionally offers the ability to paste your json data from the clipboard.  Note, we might need to detect ipads and possibly other ios devices as problematic.

## [0.16.1] - 2020-07-07

### Fixed
- Zoomed out imports were slow due to drawing of all HPWU markers on the screen.  We now simulate zoom behavior and only add markers for what's visible and based on zoom level filtering so importing 4000 objects on a zoomed out map doesn't add markers for all of them.

## [0.16.0] - 2020-07-06

### Changed
- Display POI or Inn cell scores after the data is retrieved from intel.  Previously, we did this twice, once, using the user's mapped data and later when intel was finished.  The earlier data was not always correct and
slowed down page loading.  It's better to show this later, when all the data is in and the cell score is accurate and has a side effect of speeding up the displaying of our HPWU markers on the map.
- Only calculate and draw the cell score markers for the enabled POI or Inn score.  Previously, we did both regardless of what was selected.

### Fixed
- Don't display a score if the user selects both POI and Inn score.

## [0.15.3] - 2020-07-02

### Fixed
- Improved initial page load performance (saves 250ms on chrome locally) by ensuring resetAllMarkers (which clears all hpwu markers and readds the visible ones) is run only once as it was being done both on zoomend and moveend callbacks, which can and does happen on page load.

## [0.15.2] - 2020-07-01

### Fixed
- Further improved json imports by returning early from the addStar function if the portal icon we're about to draw is offscreen.

## [0.15.1] - 2020-06-30

### Fixed
- Greatly improved json imports on chrome by skipping updateStarPortal (the function that lights up the right inn/fortress/greenhouse/n/a icon) since the portal details window (and icons) are not visible.

## [0.15.0] - 2020-06-29

### Added
- Added option to export/save only what's NOT visible on the map.  Previously, you could only export what you could see on the map or everything.  There was no option to "remove" what's visible on the screen from your export.
- Reworded some screen messages in the HPWU Actions menu for this change.

## [0.14.1] - 2020-06-29

### Fixed
- Handle neighboring cells with no poi.  Previously, the POI score wouldn't be drawn if the neighboring 8 cells had no POI.

## [0.14.0] - 2020-06-09

### Added
- Extract poi scoring function overwritable in console via: window.plugin.hpwu.calculatePoiScore

## [0.13.2] - 2020-05-11

### Fixed
- Properly catch errors loading the ZoomDisplay and log them instead of blowing up.  Currently, ZoomDisplay fails to load on IITC mobile on android but seems to work on IOS and desktop.  Now, android logs the error and continues to load the plugin.

## [0.13.1] - 2020-05-04

### Changed
- Add accesskey 9 for the inn color selection box.  You can now tap a portal, use accesskey 7 for inn and then access 9 to activate the color select and navigate using arrow keys to pick the right color.

## [0.13.0] - 2020-05-04

### Added
- Add the zoom level number to the map.
- Show the number of not classified/reviewed portals in the poi score dialog.

### Changed
- Start filtering HPWU markers at zoom 14 - same as when intel filters portals. Memory usage on a large database shows far beyond 100 MB of memory at level 14.
- When filtering markers at "zoomed out" levels 14 and below, decrease the number of HPWU markers from 150 per type (inn/fortress/greenhouse/n/a) (up to 600) to 100 (up to 400).
- POI scoring now defaults to black font color.

### Fixed
- Fixed shortcut "accesskey" for fortress/inn/greehouse/N/A:
  - Note: accesskey shortcuts are available through either: alt + accesskey, alt + shift + accesskey, OR control + alt + accesskey.
  - fortress and inn were reusing keys (f and i) used by IITC itself and didn't work.
  - N/A (nothpwu) didn't have an accesskey.
  - Now, you can use "accesskey" 5 (fortress), 6 (greenhouse), 7 (inn), 8 (N/A or nothpwu).

## [0.12.1] - 2020-04-25
### Fixed
- Fix some broken URLs in the README and the user script.

## [0.12.0] - 2020-04-24
### Changed
- Only draw the HPWU markers that are visible on screen

## [0.11.0] - 2020-04-24
### Changed
- Limit how many markers are shown of each type (150) at zoom level 13 and below.

## [0.10.2] - 2020-04-23
### Changed
- Make the user zoom in one more level to see s2 cells so it doesn't flood their screen with tiny cells.

### Fixed
- Don't classify cell POI scoring if it's disabled because not all portals are shown due to zoom.  A follow up item to release 0.9.0.

## [0.10.1] - 2020-04-22
### Fixed
- S2 action is now gone, rename the dialog

## [0.10.0] - 2020-04-22
### Changed
- Drop export S2 option from action dialog (it's unused)
- Rename save and reorder the action dialog

## [0.9.2] - 2020-04-22
### Fixed
- Fix a typo

## [0.9.1] - 2020-04-22
### Changed
- Add ALL the URLs to the user script for hopefully easier upgrades

## [0.9.0] - 2020-04-22
### Added
- Added statistics about your mapped data statistics in the actions dialog and refresh it on import/reset.
- Add option to include N/A portals in the file when saving the data on the screen.

### Fixed
- Don't try to calculate scores if intel map is not showing all portals.

## [0.8.1] - 2020-04-21
### Fixed
- Include the N/A portals in the breakdown of cell contents but indicate they're not used in the score.
- Classify the poi types and score them only once for the smallest number/largest cell.
- If POI scores is enabled, update the map grid after a data refresh. Intel map loads not classified portals after we draw the score so we need to update it after intel is finished.

## [0.8.0] - 2020-04-20
### Added
- Clicking a cell POI score shows the cell's hpwu contents.

## [0.7.1] - 2020-04-17
### Fixed
- Change dialogs to "auto" width for better mobile experience.

## [0.7.0] - 2020-04-16
### Added
- Add inn color score checking for level 15 S2 cells.  Clicking an inn score will tell if has any inn colors that don't match the algorithm.  A cell's inn score is the number of inns in it's cell + (number of inns in the surrounding 8 cells / number of surrounding cells with inns).
  - Purple if >= 20.1
  - Brown if >= 15.1
  - Pink if >= 10.1
  - Blue if >= 5.1
  - Otherwise, it should be Green.

## [0.6.1] - 2020-04-13
### Fixed
- Only calculate and show inn scores with level 15 cells.
- Don't show ? for poi score if there's unclassified portals.

## [0.6.0] - 2020-04-13
### Fixed
- Remember the last inn color selection and use it for new inns. You can now fix a cell by changing one inn to the right color and then clicking the inn icon twice as it will remember the "right" last picked color.  Hopefully, this eliminates some clicks and taps.

## [0.5.0] - 2020-04-13
### Added
- Add POI S2 cell scoring.  This is based on the inn score algorithm.  A cell's POI score is the number of inns, fortresses, greenhouses, and unclassified portals in the cell + (the number of these things in the surrounding 8 cells / number of surrounding cells with these things).
- Show the POI score for the larger cell/smaller number cell, not just level 15.

### Fixed
- Rename the user script so it will install next to lunarul's amazing original script.

[Unreleased]: https://gitlab.com/knowoptions842/hpwu-iitc/compare/v0.24.3...master
[0.24.3]: https://gitlab.com/knowoptions842/hpwu-iitc/compare/v0.24.2...v0.24.3
[0.24.2]: https://gitlab.com/knowoptions842/hpwu-iitc/compare/v0.24.1...v0.24.2
[0.24.1]: https://gitlab.com/knowoptions842/hpwu-iitc/compare/v0.24.0...v0.24.1
[0.24.0]: https://gitlab.com/knowoptions842/hpwu-iitc/compare/v0.23.8...v0.24.0
[0.23.8]: https://gitlab.com/knowoptions842/hpwu-iitc/compare/v0.23.7...v0.23.8
[0.23.7]: https://gitlab.com/knowoptions842/hpwu-iitc/compare/v0.23.6...v0.23.7
[0.23.6]: https://gitlab.com/knowoptions842/hpwu-iitc/compare/v0.23.5...v0.23.6
[0.23.5]: https://gitlab.com/knowoptions842/hpwu-iitc/compare/v0.23.4...v0.23.5
[0.23.4]: https://gitlab.com/knowoptions842/hpwu-iitc/compare/v0.23.3...v0.23.4
[0.23.3]: https://gitlab.com/knowoptions842/hpwu-iitc/compare/v0.23.2...v0.23.3
[0.23.2]: https://gitlab.com/knowoptions842/hpwu-iitc/compare/v0.23.1...v0.23.2
[0.23.1]: https://gitlab.com/knowoptions842/hpwu-iitc/compare/v0.23.0...v0.23.1
[0.23.0]: https://gitlab.com/knowoptions842/hpwu-iitc/compare/v0.22.2...v0.23.0
[0.22.2]: https://gitlab.com/knowoptions842/hpwu-iitc/compare/v0.22.1...v0.22.2
[0.22.1]: https://gitlab.com/knowoptions842/hpwu-iitc/compare/v0.22.0...v0.22.1
[0.22.0]: https://gitlab.com/knowoptions842/hpwu-iitc/compare/v0.21.1...v0.22.0
[0.21.1]: https://gitlab.com/knowoptions842/hpwu-iitc/compare/v0.21.0...v0.21.1
[0.21.0]: https://gitlab.com/knowoptions842/hpwu-iitc/compare/v0.20.0...v0.21.0
[0.20.0]: https://gitlab.com/knowoptions842/hpwu-iitc/compare/v0.19.0...v0.20.0
[0.19.0]: https://gitlab.com/knowoptions842/hpwu-iitc/compare/v0.18.0...v0.19.0
[0.18.0]: https://gitlab.com/knowoptions842/hpwu-iitc/compare/v0.17.0...v0.18.0
[0.17.0]: https://gitlab.com/knowoptions842/hpwu-iitc/compare/v0.16.2...v0.17.0
[0.16.2]: https://gitlab.com/knowoptions842/hpwu-iitc/compare/v0.16.1...v0.16.2
[0.16.1]: https://gitlab.com/knowoptions842/hpwu-iitc/compare/v0.16.0...v0.16.1
[0.16.0]: https://gitlab.com/knowoptions842/hpwu-iitc/compare/v0.15.3...v0.16.0
[0.15.3]: https://gitlab.com/knowoptions842/hpwu-iitc/compare/v0.15.2...v0.15.3
[0.15.2]: https://gitlab.com/knowoptions842/hpwu-iitc/compare/v0.15.1...v0.15.2
[0.15.1]: https://gitlab.com/knowoptions842/hpwu-iitc/compare/v0.15.0...v0.15.1
[0.15.0]: https://gitlab.com/knowoptions842/hpwu-iitc/compare/v0.14.1...v0.15.0
[0.14.1]: https://gitlab.com/knowoptions842/hpwu-iitc/compare/v0.14.0...v0.14.1
[0.14.0]: https://gitlab.com/knowoptions842/hpwu-iitc/compare/v0.13.2...v0.14.0
[0.13.2]: https://gitlab.com/knowoptions842/hpwu-iitc/compare/v0.13.1...v0.13.2
[0.13.1]: https://gitlab.com/knowoptions842/hpwu-iitc/compare/v0.13.0...v0.13.1
[0.13.0]: https://gitlab.com/knowoptions842/hpwu-iitc/compare/v0.12.1...v0.13.0
[0.12.1]: https://gitlab.com/knowoptions842/hpwu-iitc/compare/v0.12.0...v0.12.1
[0.12.0]: https://gitlab.com/knowoptions842/hpwu-iitc/compare/v0.11.0...v0.12.0
[0.11.0]: https://gitlab.com/knowoptions842/hpwu-iitc/compare/v0.10.2...v0.11.0
[0.10.2]: https://gitlab.com/knowoptions842/hpwu-iitc/compare/v0.10.1...v0.10.2
[0.10.1]: https://gitlab.com/knowoptions842/hpwu-iitc/compare/v0.10.0...v0.10.1
[0.10.0]: https://gitlab.com/knowoptions842/hpwu-iitc/compare/v0.9.2...v0.10.0
[0.9.2]: https://gitlab.com/knowoptions842/hpwu-iitc/compare/v0.9.1...v0.9.2
[0.9.1]: https://gitlab.com/knowoptions842/hpwu-iitc/compare/v0.9.0...v0.9.1
[0.9.0]: https://gitlab.com/knowoptions842/hpwu-iitc/compare/v0.8.1...v0.9.0
[0.8.1]: https://gitlab.com/knowoptions842/hpwu-iitc/compare/v0.8.0...v0.8.1
[0.8.0]: https://gitlab.com/knowoptions842/hpwu-iitc/compare/v0.7.1...v0.8.0
[0.7.1]: https://gitlab.com/knowoptions842/hpwu-iitc/compare/v0.7.0...v0.7.1
[0.7.0]: https://gitlab.com/knowoptions842/hpwu-iitc/compare/v0.6.1...v0.7.0
[0.6.1]: https://gitlab.com/knowoptions842/hpwu-iitc/compare/v0.6.0...v0.6.1
[0.6.0]: https://gitlab.com/knowoptions842/hpwu-iitc/compare/v0.5.0...v0.6.0
[0.5.0]: https://gitlab.com/knowoptions842/hpwu-iitc/-/tags/v0.5.0
